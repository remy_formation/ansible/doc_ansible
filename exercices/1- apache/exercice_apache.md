# Déploiement d'un webserver apache

But de l'exercice :

-   Introduire la ligne de commande « ansible-galaxy » pour la création et la notion de role
-   Créer les ressources nécessaires à Ansible : Inventaire, variables, playbook...
-   Introduire l'approche de l'utilisation des modules
-   Introduction à Vault
-   Introduction aux handlers
-   Sensibiliser à la gestion des variables

Temps estimé : 1H

Etapes clés :

- Initialiser le répertoire de travail
- Déployer apache (package apache2)
- Injecter un fichier index.html (/var/www/html/index.html) et un handler de redémarrage
- Passer à un fichier template et utiliser les variables

## Initialiser le répertoire de travail

**Objectif : Créer l'arborescence de travail en initialisant le nouveau role et l'inventaire**

=> Avec votre éditeur ou votre console, mettez vous dans une arborescence qui sera la racine de votre projet, comme par exemple "/home/\<user\>/ansible"

=> Initialisez un répertoire "roles" puis utilisez ansible-galaxy pour créer un role "apache"

On utilisera ansible-galaxy :

```bash
ansible-galaxy -h
```

Note - Différence entre role et collection : un role est là pour remplir une tâche précise comme installer un logiciel et déployer sa configuration. Une collection est un ensemble de ressources Ansible (role, playbooks, modules, plugins).

**Etat des lieux :**

-   **On a un un role initialisé, vide pour l'instant**

## Déployer apache

**Objectif : Création d'un playbook pour déployer un serveur apache sur le serveur, sécurisation de l'utilisation du become.**

=> Créez le playbook playbook_apache.yml, il ne possède qu'une seule tâche : il invoque le role apache pour les membres du groupe "client". On aura besoins des droits sudo.

=> Invoquez votre playbook.

=> pas de retour, pourquoi ? 

=> Pour avoir un début de retour et valider que la mécanique est bonne, insérer une tâche dans le playbook avant l'appel du role: faire un ping à destination des "client". Pour ça on utilisera le module "ping".

=> Ajouter une tache au role, se renseigner sur la doc du module « apt », on installera "apache2". Exécuter le playbook.

=> Trouvez le paramètre pour renseigner le mdp sudo dans la CLI ansible-playbook !

=> A la première exécution, on a eu les logs d'installation. Lancer une seconde exécution. Qu'es ce qui change ?  Le résultat est-il normal ?

=> Rendez vous sur http://<ip_client>/ pour vérifier que le webserveur est up.

=> Passage par vault et run du mdp sans mode interactif (Je vous laisse la fiche de correction, ça ne s'invente pas 😉)

Par contre, devoir rentrer le mdp à chaque fois, ce n'est pas idéale. Du coup, on va créer notre première variable pour gérer ça :

```bash
mkdir -p group_vars/all
ansible-vault create group_vars/all/vaulted
# Mettez formationvault en mdp
```

En contenu, on met la variable "ansible_become_pass" avec le mdp root de votre VM.

Sur le serveur, on va créer un vault.txt qui garde la clé :

```bash
echo "formationvault" > vault.txt
```

On peut discuter du fait qu'on a un mdp en clair qui se promène, mais au moins je peux tout envoyer sur git (à l'exception du .txt contenant le mdp) sans problème. Et pour récupérer le mdp du vault, il faut qu'un environnement de prod soit compromis. Dans une chaine CI, on peut générer le fichier à la volée et le supprimer tout de suite après pour éviter d'avoir le txt qui se promène, en utilisant d'autres outils prévu pour ça.

```bash
ansible-playbook playbook_apache.yml -i inventory --vault-password-file=vault.txt
```

Et maintenant, pour être tranquille avec les arguments et éviter de devoir à chaque fois préciser le "-i" et "vault-password-file" vous pouvez créer un fichier "ansible.cfg"

```bash
# Générer un ansible-config avec tout de commenté :
ansible-config init --disabled > ansible.cfg

# Trouvez les clés, supprimez le ';' et renseigner les valeurs suivantes
inventory=inventory/formation
vault_password_file=vault.txt

# toutes les clées ici : https://docs.ansible.com/ansible/latest/reference_appendices/config.html
```

Note 1 : ansible.cfg peut être chargé depuis : une variable d'env, le répertoire courant, depuis un fichier dans le home : ~/.ansible.cfg si on veut qu'il soit transverse, depuis /etc/ansible/ansible.cfg

Note 2 : On peut aussi déposer l'inventaire dans /etc/ansible/hosts pour l'utiliser de manière transverse, peut importe son répertoire

**Etat des lieux :**

-   **On a un serveur apache installé par un role**
-   **On a vue un usage d'ansible vault**

## Injecter un fichier index.html et un handler de redémarrage

**Objectif : Rajouter un fichier qui servira de page principale. Le rendre customisable via des variables.**

=>   Rajouter un "fichier" index.html dans le role, qui servira à écraser le retour de base d'apache. Le contenu n'est pas important pour le moment, si vous n'avez pas d'idées : '<p>Coucou</p>'

=> Rajouter une tâche au rôle. On utilisera : le module "copy" d'ansible. Le répertoire de destination de la racine d'un webserver apache par défaut est : "/var/www/html/index.html"

=> Relancer le playbook. Vous devriez constater un changement

=> Faire un nouveau Curl / rendez vous sur http://<ip_client>/, pour vérifier le résultat

=> Créer un handler pour redémarrer apache si le fichier change. N'oubliez pas que vous pouvez à tout moment accéder aux slides.

=> Si rien ne s'est passé, je rappel que le handler n'est pas appelé tant qu'il n'y a pas de modifications faites. Modifier index.html et relancer.

**Etat des lieux :**

-   **On a maintenant un fichier index.html qui est déployé**
-   **On a un handler qui permet de restart le serveur à la moindre modification**

## Passer à un fichier template et utiliser les variables

**Objectif : utiliser un fichier template pour l'index.html et tester la précédence des variables.**

=> Créer un template : le fichier roles/apache/templates/index.html.j2, le contenu est libre, mais il doit contenir la variable "var_index"

=> Initialiser la variable "var_index" par défaut de role

 => Modifier la tache pour utiliser module plus adapté (voir le module template) 

=> Redéployer avec le playbook

=> Faire un nouveau Curl pour vérifier le résultat

=> On a défini une valeur de base pour le rôle. Maintenant, on va uniquement manipuler des variables pour changer l'index.html généré par Ansible. Ajouter la variable en tant que variable « de groupe » et pour « tous les groupes » (en lui donnant une valeur différente). Redéployer Ensuite et vérifier le résultat.

=> Maintenant, on va définir la même variable, avec une troisième valeur, comme variable « de groupe » mais pour le groupe « client » spécifiquement. Redéployer Ensuite et vérifier le résultat.

=> Commande « magique » pour lister les variables en fonction des hosts : ansible-inventory -i inventory \--graph --vars , on voit une différence entre ansible-client et ansible-serveur, normale ? Explications ?

```bash
@all:
|--@ungrouped:
|--@vm:
| |--@client:
| | |--ansible-client
| | | |--{ansible_become_pass = remy}
| | | |--{var_index = remy on avait dit}
| |--@serveur:
| | |--ansible-serveur
| | | |--{ansible_become_pass = remy}
| | | |--{var_index = Nicolas}
```

=> Maintenant, on va changer la variable via les « variables d'hote ». Donner une valeur spécifique pour la même variable, au serveur ansible. Puis vérifiez la différence avec la commande ansible-inventory

=> Et enfin, on change une dernière fois avec l'option la plus prioritaire : l'extra-var (voir la commande ansible-playbook)

**Etat des lieux :**

-   **L'index.html est maintenant templatisé et prêt à prendre de multiples valeurs en fonction des environnements et hosts**

Tree final:

![exo_apache_final_tree](..\images\exo_apache_final_tree.png)