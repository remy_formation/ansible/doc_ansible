# Corrigé exercice : Déploiement d'un webserver apache

But de l'exercice :

-   Introduire la ligne de commande « ansible-galaxy » pour la création et la notion de role
-   Créer les ressources nécessaires à Ansible : Inventaire, variables, playbook...
-   Introduire l'approche de l'utilisation des modules
-   Introduction à Vault
-   Introduction aux handlers
-   Sensibiliser à la gestion des variables

Temps estimé : 1H

Etapes clés :
- Initialiser le répertoire de travail
- Déployer apache (package apache2)
- Injecter un fichier index.html (/var/www/html/index.html) et un handler de redémarrage
- Passer à un fichier template et utiliser les variables

## Initialiser le répertoire de travail

**Objectif : Créer l'arborescence de travail en initialisant le nouveau role et l'inventaire**

=> Avec votre éditeur ou votre console, mettez vous dans une arborescence qui sera la racine de votre projet, comme par exemple "/home/\<user\>/ansible"

=> Initialiser un répertoire "roles" puis utiliser ansible-galaxy pour créer un role "apache"

On utilisera ansible-galaxy :

```bash
ansible-galaxy -h
```

Note - Différence entre role et collection : un role est là pour remplir une tâche précise comme installer un logiciel et déployer sa configuration. Une collection est un ensemble de ressources Ansible (role, playbooks, modules, plugins).

```bash 
ansible-galaxy role -h
```

Voir les différentes options disponibles:
on peut se logger, exporter, importer etc...

Initialiser le role dans roles/ :

```bash 
mkdir roles
cd roles
ansible-galaxy role init apache
```

=> Créer l'inventaire "formation" dans le répertoire "inventory". On utilisera les groupes suivants : 

- client pour la VM ansible-client
- serveur pour la VM ansible-serveur. Note on rajoutera les options :  ansible_host=localhost ansible_connection=local
- vm : contient les deux groupes précédents(pour la vm client, la vm serveur et les groupes)

Revenir à la racine du projet et créer l'inventaire

```bash
mkdir inventory
touch inventory/formation
```

On le remplie avec nos deux hosts

```bash
vim inventory/formation
```

Contenu du fichier :
```ini
[client]
ansible-client

[serveur]
ansible-serveur ansible_host=localhost ansible_connection=local

[vm:children]
client
serveur
```

=>Vérifier que l'inventaire fonctionne avec un ping (commande adhoc ansible)

Et on peut vérifier que ça fonctionne bien avec :

```
ansible <groupe_inventaire> -i inventory -m ping
```

**Etat des lieux :**

-   **On a un inventaire prêt à l'utilisation**
-   **On a un un role initialisé, vide pour l'instant**

## Déployer apache

**Objectif : Création d'un playbook pour déployer un serveur apache sur le serveur, sécurisation de l'utilisation du become.**

=> Créer le playbook playbook_apache.yml, il ne possède qu'une seule tâche : il invoque le role apache pour les membres du groupe "client". On aura besoins des droits sudo.

On crée le playbook:
```
touch playbook_apache.yml
```

Et on le remplie avec l'appel à notre rôle :
```yaml
- name: Deploy apache
  hosts: client
  become: yes
  gather_facts: false
  roles:
    - apache
```

=> Invoquer votre playbook.

```bash
ansible-playbook --help
```

```bash
ansible-playbook playbook_apache.yml -i inventory
```
=> pas de retour, pourquoi ? 

Car on n'a rien dans task pour le moment.

=> Pour avoir un début de retour et valider que la mécanique est bonne, insérer une tâche dans le playbook avant l'appel du role: faire un ping à destination des "client". Pour ça on utilisera le module "ping".

Pour valider l'inventaire on peut faire un ping avant :

```yaml
- name: ping
  hosts: client
  gather_facts: false
  tasks:
    - name: ping hosts
      ping:
```

L'output devrait être plus parlant.

=> Ajouter une tache au role, se renseigner sur la doc du module « apt », on installera "apache2". Exécuter le playbook.

On peut maintenant ajouter une tache au rôle :

```bash
vim roles/apache/tasks/main.yml
```

Contenu :
```yaml
- name: Install apache
  apt:
    name: apache2
    state: latest
```

Run le playbook :
```bash
ansible-playbook playbook_apache.yml -i inventory
```

=> Trouver le paramètre pour renseigner le mdp sudo dans la CLI ansible-playbook

Problème, on n'a pas les droits sudo, on rajoute l'option : --ask-become-pass ou -K

```bash
ansible-playbook playbook_apache.yml -i inventory -K
```

=> A la première exécution, on a eu les logs d'installation. Lancer une seconde exécution. Qu'es ce qui change ?  Le résultat est-il normal ?

On run 2 fois : on n'a pas de changement : normale, on est sur du déclaratif donc pas besoins de modifications

=> Passage par vault et run du mdp sans mode interactif (Je vous laisse la fiche de correction, ça ne s'invente pas 😉)

Par contre, devoir rentrer le mdp à chaque fois, ce n'est pas idéale. Du coup, on va créer notre première variable pour gérer ça :

```bash
mkdir -p group_vars/all
ansible-vault create group_vars/all/vaulted
```

En contenu, on met la variable "ansible_become_pass" avec le mdp root de votre VM.

On crée le mdp en local :

```bash
echo "vault" > vault.txt
```

On peut discuter du fait qu'on a un mdp en clair qui se promène, mais au moins je peux tout envoyer sur git (à l'exception du .txt contenant le mdp) sans problème. Et pour récupérer le mdp du vault, il faut qu'un environnement de prod soit compromis. Dans une chaine CI, on peut générer le fichier à la volée et le supprimer tout de suite après pour éviter d'avoir le txt qui se promène, en utilisant d'autres outils prévu pour ça.

```bash
ansible-playbook playbook_apache.yml -i inventory --vault-password-file=vault.txt
```

Pour être tranquille avec les arguments et éviter de devoir à chaque fois précisier le "-i" et "vault-password-file" vous pouvez créer un fichier "ansible.cfg"

```cfg
inventory=inventory
DEFAULT_VAULT_PASSWORD_FILE=vault.txt
```

=> On peut maintenant requêter la VM Client pour vérifier qu'apache tourne : curl http://ansible-client/ 

**Etat des lieux :**

-   **On a un serveur apache installé par un role**
-   **On a vue un usage d'ansible vault**

## Injecter un fichier index.html et un handler de redémarrage

**Objectif : Rajouter un fichier qui servira de page principale. Le rendre customisable via des variables.**

=>   Rajouter un "fichier" dans le role, qui servira à écraser le retour de base d'apache. Le contenu n'est pas important pour le moment, si vous n'avez pas d'idées : '<p>Coucou</p>'

Pour ça, on va commencer par créer notre fichier dans le role :
```bash
echo "<p> This is my index_file </p>" > roles/apache/files/index.html
```

=> Rajouter une tâche au rôle. On utilisera : le module "copy" d'ansible. Le répertoire de destination de la racine d'un webserver apache par défaut est : "/var/www/html/index.html"

On rajoute une tache :

```bash
- name: deploy index.html
  copy:
    src: index.html
    dest: /var/www/html/index.html
    owner: root
    group: root
    mode: '0644'
```

=> Relancer le playbook. Vous devriez constater un changement

```bash
ansible-playbook playbook_apache.yml -i inventory --vault-password-file=vault.txt
```

=> Faire un nouveau Curl pour vérifier le résultat

Nouveau curl, nouveau résultat normalement.

=> Créer un handler pour redémarrer apache si le fichier change. N'oubliez pas que vous pouvez à tout moment accéder aux slides.

On a « de la chance » qu'apache soit dynamique sur sa lecture des fichier html. Pour l'exercice, on va tout de même commander un redémarrage du serveur si le fichier à changer.

On modifies la tache:

```bash
- name: deploy index.html
  copy:
    src: index.html
    dest: /var/www/html/index.html
    owner: root
    group: root
    mode: '0644'
  notify:
    - restart apache
```

On crée le Handler :

```bash
vim roles/apache/handlers/main.yml
```
```bash
- name: restart apache
  service:
    name: apache2
    state: restarted
```

On run :
```bash
ansible-playbook playbook_apache.yml -i inventory --vault-password-file=vault.txt
```

=> Si rien ne s'est passé, je rappel que le handler n'est pas appelé tant qu'il n'y a pas de modifications faites. Modifier index.html et relancer.

Modifier index.html d'une quelconque manière

```bash
ansible-playbook playbook_apache.yml -i inventory --vault-password-file=vault.txt
```

On a bien le handler appelé :
```bash
RUNNING HANDLER [apache : restart apache] *******************
changed: [ansible_serveur]
```

**Etat des lieux :**
-   **On a maintenant un fichier index.html qui est déployé**
-   **On a un handler qui permet de restart le serveur à la moindre modification**

## Passer à un fichier template et utiliser les variables

**Objectif : utiliser un fichier template pour l'index.html et tester la précédence des variables.**

=> Créer un template : le fichier roles/apache/templates/index.html.j2, le contenu est libre, mais il doit contenir la variable "var_index"

```bash
echo "<p> Tout le monde sait que le meilleur c'est {{ var_index }} <p>" > roles/apache/templates/index.html.j2
```

=> Initialiser la variable "var_index" par défaut de role

créer la variable var_index

```bash
echo 'var_index: "remy"' > roles/apache/defaults/main.yml
```

 => Modifier la tache pour utiliser module plus adapté (voir le module template) 

Modifier la tache :

```bash
- name: deploy index.html
  template:
    src: index.html.j2
    dest: /var/www/html/index.html
    owner: root
    group: root
    mode: '0644'
  notify:
    - restart apache
```
=> Redéployer avec le playbook

```bash
ansible-playbook playbook_apache.yml -i inventory --vault-password-file=vault.txt
```

=> Faire un nouveau Curl pour vérifier le résultat

Nouveau curl, nouveau résultat normalement.

=> On a défini une valeur de base pour le rôle. Maintenant, on va uniquement manipuler des variables pour changer l'index.html généré par Ansible. Ajouter la variable en tant que variable « de groupe » et pour « tous les groupes » (en lui donnant une valeur différente). Redéployer Ensuite et vérifier le résultat.

```bash
mkdir -p group_var/all
echo 'var_index: "Nicolas"' > group_vars/all/my_var.yml
```

On re-déploie, on a bien une modif de la variable

=> Maintenant, on va définir la même variable, avec une troisième valeur, comme variable « de groupe » mais pour le groupe « client » spécifiquement. Redéployer Ensuite et vérifier le résultat.

On crée le dossier qui va bien :

```bash
mkdir group_vars/client
```

On insère la variable

```bash
echo 'var_index: "remy on avait dit"' > group_vars/client/my_var.yml
```

=> Commande « magique » pour lister les variables en fonction des hosts : ansible-inventory -i inventory \--graph --vars , on voit une différence entre ansible-client et ansible-serveur, normale ? Explications ?

```bash
@all:
|--@ungrouped:
|--@vm:
| |--@client:
| | |--ansible-client
| | | |--{ansible_become_pass = remy}
| | | |--{var_index = remy on avait dit}
| |--@serveur:
| | |--ansible-serveur
| | | |--{ansible_become_pass = remy}
| | | |--{var_index = Nicolas}
```

Tout simplement qu'ansible-serveur profite du group_vars/all tandis qu'ansible-client va au plus spécifique, donc group_vars/client

=> Maintenant, on va changer la variable via les « variables d'hote ». Donner une valeur spécifique pour la même variable, au serveur ansible. Puis vérifiez la différence avec la commande ansible-inventory

```bash
mkdir host_vars
echo 'var_index: "definitivement Nicolas..."' > host_vars/ansible-serveur.yml
```
```bash
ansible-inventory -i inventory --graph --vars --vault-password-file=vault.txt
```

```bash
@all:
|--@ungrouped:
|--@vm:
| |--@client:
| | |--ansible-client
| | | |--{ansible_become_pass = remy}
| | | |--{var_index = remy on avait dit}
| |--@serveur:
| | |--ansible-serveur
| | | |--{ansible_become_pass = remy}
| | | |--{var_index = definitivement Nicolas...}
```

=> Et enfin, on change une dernière fois avec l'option la plus prioritaire : l'extra-var (voir la commande ansible-playbook)

```bash
ansible-playbook playbook_apache.yml -i inventory -e "var_index=grosminet" --vault-password-file=vault.txt 
```

**Etat des lieux :**
-   **L'index.html est maintenant templatisé et prêt à prendre de multiples valeurs en fonction des environnements et hosts**

Tree final:

![exo_apache_final_tree](..\images\exo_apache_final_tree.png)
