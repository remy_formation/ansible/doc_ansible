# Setup des VMs et premiers pas avec Ansible

But de l'exercice :

-   Se familiariser avec l'IDE de la formation, et valider son intégrité
-   Faire quelques configurations préliminaires pour la suite des exercices
-   Se familiariser avec la notion de module et découvrir les modules fondamentaux

Temps estimé : 45Min

## Accéder à L'IDE de son serveur

**Objectif : Vérifier que l'environnement de TP est opérationnel**

=> Connectez vous à l'adresse http://<ip_de_votre_serveur>:8080/ 

=> Un mot de passe est demandé pour accéder à code-server, il correspond à celui fourni dans l'archive par votre formateur

=> Vous devriez arriver sur une interface VSCode. Avec le bouton d'exploration en haut à gauche (les trois lignes parallèles), ouvrez un terminal : C'est un terminal directement sur votre serveur.

=> Créez un répertoire ansible dans lequel on travaillera :

```bash
mkdir -p ~/ansible
```

=> Vérifiez que vous avez accès aux commandes suivantes : 

```bash
ansible -h
ansible-playbook -h
ansible-galaxy -h
ansible-doc -h
```

=> Vérifiez que vous avez la clé SSH "ansible_key" pour accéder à votre serveur client sur lequel on effectuera les opérations

```bash
ls -al ~/.ssh
total 24
drwx------ 2 ubuntu ubuntu 4096 Nov 17 19:02 .
drwxr-x--- 8 ubuntu ubuntu 4096 Nov 20 16:27 ..
-rw------- 1 ubuntu ubuntu 2611 Nov 17 18:57 ansible_key
-rw------- 1 ubuntu ubuntu  575 Nov 20 14:21 authorized_keys
-rw------- 1 ubuntu ubuntu 1956 Nov 17 19:02 known_hosts
-rw------- 1 ubuntu ubuntu 1120 Nov 17 19:02 known_hosts.old
```

=> Tentez une connection sur le serveur client :

```bash
ssh ubuntu@<ip_client> -i ~/.ssh/ansible_key 

# Une fois connecté, pour revenir sur le serveur
exit
```

=> Enfin, vous pouvez ouvrir le répertoire "ansible" en tant que nouvel espace de travail pour la suite si vous préférez un IDE pour éditer les fichiers.

## Inventaire et commandes AdHoc

**Objectif : Découvrir les notions d'inventaires et de modules**

=> Dans le répertoire ~/ansible, Créez l'inventaire "formation" dans le répertoire "inventory". On utilisera les groupes suivants : 

- client pour la VM ansible-client
- serveur pour la VM ansible-serveur. Note on rajoutera les options :  ansible_host=localhost ansible_connection=local
- vm : contient les deux groupes précédents(pour la vm client, la vm serveur et les groupes)

=> Essayez d'utiliser le module de ping en AdHoc pour vérifier la connexion aux deux serveurs. Un des deux devrait être en erreur.

Note: La commande ansible-doc est là pour vous aider. Sinon rendez vous sur la documentation en ligne !

=> L'erreur provient d'un problème de clé SSH. On va donc rajouter un paramètre dans l'inventaire pour spécifier la clé SSH à utiliser : 

```
ansible_ssh_private_key_file=~/.ssh/ansible_key
```

=> Maintenant, avec le module "apt", installez le package "tree". Il vous faudra les droits sudo, donc un --become" est nécessaire dans la ligne de commande.  Exécutez la commande une première fois, puis une seconde fois. Quelles différences ? Pourquoi ? 

=> Si vous voulez d'autres packages comme un éditeur dans le terminal (Nano), n'hésitez pas à en profiter pour les installer !

## Préparons un compte ansible...

**Dans un environnement "normal" vous avez souvent un compte avec des droits limités pour ansible. On veut éviter que celui-ci casse tout sans aucune limite sur une erreur !**

=> Avec le module "user": Créez un utilisateur "ansible" sur le client, peu importe le mot de passe. 

=> Avec le module "file" : Créez le répertoire "/home/ansible/.ssh", avec en owner/group "ansible". Vous aurez besoin notamment de l'option "state=directory".

=> Avec le module "copy": Copiez le fichier .ssh/authorized_key vers le client UNIQUEMENT, dans "/home/ansible/.ssh/authorized_key", L'opération se fait en deux temps : création du répertoire .ssh, puis création du fichier.

Maintenant essayez de vous connecter avec la clé Ansible : 

```bash
# Se connecter au client en tant qu'ansible
ssh ansible@<ip_client> -i ~/.ssh/ansible_key

# Revenir la VM serveur : 
exit
```

=> Reconnectez vous avec le compte Ubuntu, et mettez un mot de passe au compte ansible (Note : on peut le faire depuis ansible, mais c'est compliqué pour peu de chose...)

```bash
# Se connecter au client en tant qu'ubuntu
ssh ubuntu@<ip_client> -i ~/.ssh/ansible_key

sudo passwd ansible
# Mettez formationansible en mot de passe

# Donnez les droits sudoers :
usermod -aG sudo ansible

```

=> Mettez à jour l'inventaire pour utiliser le nouveau compte "ansible" pour le client (nom de la variable : ansible_user)

=> Avec le module "command": Exécutez un "whoami" pour vérifier que vous utilisez bien le nouveau compte ansible

```bash
ansible client -m "command" -a "whoami" -i inventory/formation
```

=> Dernier appel de module avant de passer aux choses sérieuses : avec le module "setup" : Récupérez les "facts" de votre serveur client