# Objectifs
Plusieurs rendus sont attendus à la fin de ce TP :
- Installation de Mariadb sur le serveur ansible-client via un rôle
- Création d'une ou plusieurs bases de données
- Ajout d'utilisateur ayant les droits sur les bases précédemment créée.

Les utilisateurs et des bases de données à créer devront être lister dans des variables présentes dans le host_vars. 

# Comment s'y prendre ?

1. Comme dans le TP précédent, il faut créer un role mariadb. Le rôle se créer via la commande `ansible-galaxy`.

2. Créer un playbook faisant appel au role mariadb

3. Afin de simplifier la visibilité du code, il est d'usage de realiser des **include** des fichier yaml contenant du code. Par exemple, dans le fichier `role/mariadb/tasks/main.yml`, ajouter les lignes suivantes :
```yaml
---
- import_tasks: install_mariadb.yml
```
Créer ensuite le fichier `install_mariadb.yml` dans le répertoire tasks. Pour l'instant, la suite du script Ansible se fera donc dans ce document.

4. Dans `install_mariadb.yml`, deux étapes sont nécessaire pour installer et paraméter mysql. L'installation se fait via le module `ansible.builtin.apt`, la documentation est disponible sur internet (il sera peut être nécessaire de mettre à jour le cache). 

Plusieurs paquets sont néessaires :
  * mariadb-client
  * mariadb-server
  * python3-pymysql

Pour l'occasion, on utilisera une boucle "with_items", qui permet de remplacer le nom du module par la variable {{item}} puis de définir en dessous une liste d'élément, favorisant grandement la lisiblité

4.bis Mariadb ne semble pas initialiser le répertoire "/var/log/mysql" et démarre en erreur. Il faut donc le créer avec Ansible en spécifiant le owner/group mysql et le mode "0750"

5. La configuration de mariadb se fait via un fichier de configuration à placer dans le repertoire `/etc/mysql/mariadb.conf.d/50-server.cnf`. Voici à quoi il peut ressembler :
```ini
# The MariaDB database server configuration file.
#

[mysqld]
user        = mysql
pid_file    = /var/run/mysqld/mysqld.pid
socket      = /var/run/mysqld/mysqld.sock
port        = 3306
basedir     = /usr

skip-external-locking
key_buffer_size                 = 16M
max_allowed_packet              = 16M
thread_stack                    = 192K
thread_cache_size               = 8
long_query_time                 = 2
max_binlog_size                 = 100M
bind_address                    = 0.0.0.0
general_log_file                = /var/log/mysql/mysql.log
log_error                       = /var/log/mysql/error.log
slow-query-log-file             = /var/log/mysql/mysql-slow.log
innodb_flush_method             = O_DIRECT
server-id                       = 1
datadir                         = /var/lib/mysql
tmpdir                          = /tmp
log_bin                         = /var/log/mysql/mysql-bin.log
max_connections                 = 151
innodb_buffer_pool_size         = 8M
innodb_buffer_pool_instances    = 8
innodb_log_buffer_size          = 8M
innodb_log_file_size            = 256M
```
Copier/coller cette configuration et placer la dans le fichier `roles/mariadb/files/50-server.cnf`. Utiliser ensuite le module `ansible.builtin.copy` pour le déposer sur le serveur distant.

6. L'installation de mariadb est terminée, mais un détail reste à régler. Si une modification est réaliser dans la configuration de mariadb, il faut redémarrer le service mariadb. Créer un `handler` dans la même logique que celui créé pour le role apache2.

7. L'installation de mariadb est terminée, il est maintenant temps de gérer la partie utilisateur. Retourner dans le fichier `tasks/main.yml` et importer un nouveau fichier correspondant au tâche à réaliser pour créer les utilisateurs.

8. Lisez la documentation du module `community.mysql.mysql_user` et essayer de l'utiliser pour créer votre premier utilisateur. Un plus serait créer une liste d'utilisateur dans le fichier `host_vars/<ip_client>.yml`. Voici à quoi la déclaration de ces variables pourraient ressembler :
```yaml
---
bdd_user:
  - { name: user1, password: P@ssword, host: 'localhost', priv: 'bdd1.*:ALL' }
  - { name: user2, password: P@ssword, host: 'localhost', priv: 'bdd2.*:ALL' }
```
Il est possible de boucler sur la liste des utilisateurs en s'inspirant des instructions suivantes :
```yaml
- name: Add db account
  community.mysql.mysql_user:
    name: "{{ item.name }}"
loop: "{{ bdd_user |flatten (levels=1) }}"
```

Note : pour la connexion on souhaite utiliser le user "root" de MariaDB, pour ça on passera par le "unix_socket" qui se trouve ici : /var/run/mysqld/mysqld.sock

9. Oups, nous venons de mettre un bug dans notre script. En effet, si nous lançons le script mais qu'aucun utilisateur n'est déclaré, une erreur va bloquer la fin de exécution du script. Pour se prémunir de cette erreur, utiliser la condition `when` pour vérifier que la variable bdd_user est bien définie.

10. Pour vérifier que tout est bien installé, vous pouvez vous connecter en ssh sur le serveur client et faire les commandes suivantes : 

```bash
ssh ubuntu@<ip_serveur> -i ~/.ssh/ansible_key 
sudo mariadb
# Dans l'outil :
MariaDB [(none)]> SELECT user from mysql.user;

```



11. La gestion des utilisateurs est terminée, il est enfin temps de créer notre première base de données. Pour se faire, et comme les autres fois, retourner dans le fichier `tasks/main.yml` et ajouter l'import d'un nouveau fichier permettant la création de base de données.

12. Lisez la documentation du module `community.mysql.mysql_db` et comme pour la partie utilisateur, ajoutez les variables suivantes :

```yaml
bdd_name:
  - bdd1
  - bdd2
```

Même combat : il est possible que vous ayez besoin de passer par le unix_socket

13. Pour vérifier que les bases sont bien présentes, vous pouvez à nouveau vous connecter en SSH:

```bash
ssh ubuntu@<ip_serveur> -i ~/.ssh/ansible_key 
sudo mariadb-show *
```



14. Bravo ! Nous sommes au dernier point de notre TP mariadb ! Un dernière chose, lors de la création des utilisateurs bdd, vous avez indiqué un mot en clair dans la configuration. Pour corriger cette faille de sécurité, comme dans le TP apache2, il est possible d'utiliser ansible-vault.

```bash
ansible-vault create <path du chemin pour le stockage des secrets>
```
Indiquer un mot de passe pour chiffrer le fichier vault et créer une variable contenant les mot de passe des utilisateurs de base de données.

Corriger ensuite la variable `bdd_user` pour utiliser le vault:
```yaml
---
bdd_user:
  - { name: user1, password: "{{vault_password_user1}}", host: 'localhost', priv: 'bdd1.*:ALL' }
  - { name: user2, password: "{{vault_password_user2}}", host: 'localhost', priv: 'bdd2.*:ALL' }
```
Enfin, pour que cette variable soit connue par ansible lors de l'appel du playbook, ajoutez dans le fichier `playbooks/anisble-client.yml` la présence d'un nouveau fichier de variables :
```yml
vars_files:
  - <path du chemin pour le stockage des secrets>
```

---

# Vous avez terminé et il vous reste du temps ? Un dernier TP est disponible
Pour compléter la stack apache2 et mariadb, nous vous proposons d'installer PhpMyAdmin.



Merci à Nicolas Vallé pour avoir fourni l'exercice !