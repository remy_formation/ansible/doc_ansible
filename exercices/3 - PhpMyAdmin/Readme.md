# Objectifs
Plusieurs rendus sont attendus à la fin de ce TP :
- Installation de PhpMyAdmin depuis les sources
- Création de la configuration

Des varibales sont à utiliser pour définir le nom DNS pour joindre phpmyadmin, pour restreindre l'accès au site via des IP, pour définir les serveurs sur lesquel PhpMyadmin peut se connecter pour atteindre des bases de données.

Les étapes d'installation de l'outils sont disponibles [ici](https://docs.phpmyadmin.net/fr/latest/setup.html#quick-install-1)

# Comment s'y prendre ?

1. Créer le rôle grâce à la commande `ansible-galaxy`
2. Se positionner dans les tasks du rôle et créer deux fichiers. Le premier contiendra les étapes d'installation, le second les étapes de configuration.

# Installation

1. L'installation de phpmyadmin se réalise en trois actions. La création d'un dossier allant contenir l'archive PhpMyAdmin, le téléchargement et le dezippage de l'archive et l'installation des paquets php nécessaires au bon fonctionnement de l'application. Pour vous aidez, regarder les modules `ansible.builtin.file`, `ansible.builtin.unarchive` et `ansible.builtin.apt`. 

  1.1 Créez le répertoire : /usr/share/phpmyadmin

  1.2 Récupérez la dernière version de phpMyAdmin : https://www.phpmyadmin.net/downloads/phpMyAdmin-latest-all-languages.tar.gz 

  1.3 Les paquets php à télécharger sont les suvants :

  * libapache2-mod-php8.3
  * php8.3
  * php8.3-common
  * php8.3-curl
  * php8.3-mysqli
  * php8.3-mbstring

# Configuration

1. La mise en place d'un nouveau site web (phpmyadmin) nécessite des ajustements de configuration apache. Dans le role php, mettez à jour le fichier "/etc/apache2/ports.conf" (Exemple si dessous) pour rajouter un "listen" sur un port (81) configuré dans les variables "host_vars" (nom de variable: phpmyadmin_listen_port)

```
# If you just change the port or add more ports here, you will likely also
# have to change the VirtualHost statement in
# /etc/apache2/sites-enabled/000-default.conf

Listen 80

<IfModule ssl_module>
        Listen 443
</IfModule>

<IfModule mod_gnutls.c>
        Listen 443
</IfModule>
```



2. Le redémarrage du service apache2 sera nécessaire pour le role phpmyadmin. Créez un `handler`et rajoutez le à l'état de configuration des ports.

3. Le template ci-dessous vous permettra de créer un vhost pour apache2 (ce qui fais le lien entre l'arborescence locale et ce qui est exposé en web site), il est a installer dans le répertoire `/etc/apache2/site-available`

```
<VirtualHost *:{{ phpmyadmin_listen_port }}>
    ServerAdmin webmaster@localhost
    DocumentRoot /usr/share/phpmyadmin
    ServerName ???
    ServerAlias ???

    <Directory /usr/share/phpmyadmin>
        Options FollowSymLinks
        DirectoryIndex index.php
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/phpmyadmin_error.log
    CustomLog ${APACHE_LOG_DIR}/phpmyadmin_access.log combined
</VirtualHost>
```
Il vous manque deux informations que vous devez charger dynamiquement : le ServerName (qui correspond au hostname) et le ServerAlias (qui correpsond à l'adresse IP). Une se trouve dans les variables "spéciales" d'Ansible, l'autre dans le gather_fact. Trouvez les. 

4. Une modification de configuration d'apache nécessite obligatoirement son redémarrage. Positionner un `handler` à la fin de la tâche précédente.

5. Vous n'avez pas encore utilisé les tags, mais c'est le bon moment : grâce a cette notion vous allez pouvoir, à travers la CLI choisir d'exécuter l'intégralité, ou seulement une partie de vos tasks. Faite la configuration nécessaire pour pourvoir exécuter soit la partie installation, soit la partie configuration ou les deux d'un coups. 
6. Revenons à phpmyadmin : un paramètre de sécurité est attendu le fichier de configuration PhpMyAdmin. Ce paramètre se nomme blowfish. Voici les étapes pour le générer et l'enregister dans une variable :

```yaml
- name: Generate blowfish_secret
  ansible.builtin.shell: head /dev/urandom | tr -dc 'A-Za-z0-9' | head -c 32 ; echo ''
  register: blowfish
  changed_when: False

- ansible.builtin.set_fact:
    blowfish_secret: "{{ blowfish.stdout }}"
```
La module `shell` permet d'utiliser les commandes linux comme dans un terminal. Nous générons ici une chaine aléatoire de 32 caractères qui sera ensuite enregistrée dans la variable blowfish. Cette variable est une liste contenant toutes les informations exécution de la commande shell. Ici, seul la sortie standard nous intéresse et, dans le deuxième tâche, elle est récupérée et stockée dans la variable blowfish_secret.

7. Un deuxième template est à déployer dans le répertoire `/usr/share/phpmyadmin/config.inc.php`. Il contient les informations de configuration de PhpMyAdmin :

```php
<?php

/* Servers configuration */
$i = 0;
{% for host in phpmyadmin_hosts_list %}
/* Server: {{ host.name | default('localhost') }} */
$i++;
$cfg['Servers'][$i]['verbose'] = '{{ host.name }}';
$cfg['Servers'][$i]['host'] = '{{ host.ip | default('localhost') }}';
$cfg['Servers'][$i]['port'] = '{{ host.port | default('3306') }}';
$cfg['Servers'][$i]['socket'] = '';
$cfg['Servers'][$i]['auth_type'] = 'cookie';
$cfg['Servers'][$i]['user'] = '';
$cfg['Servers'][$i]['password'] = '';
$cfg['Servers'][$i]['AllowNoPassword'] = false;
$cfg['Servers'][$i]['AllowRoot'] = false;

{% endfor %}

/* End of servers configuration */

$cfg['blowfish_secret'] = "{{ blowfish_secret }}";
$cfg['UploadDir'] = '';
$cfg['SaveDir'] = '';
$cfg['BZipDump'] = false;
$cfg['DefaultLang'] = 'en';
$cfg['ServerDefault'] = 1;
$cfg['TempDir'] = '/tmp';
$cfg['VersionCheck'] = false;
$cfg['ShowPhpInfo'] = false;
$cfg['PmaNoRelation_DisableWarning'] = true;
$cfg['ShowServerInfo'] = false;
$cfg['NavigationTreeEnableGrouping'] = false;
?>
```
Repérez les variables utilisées lors dans ce template et ajoutez les dans le fichier `host_vars/ansible-client.yml`.

8. PhpMyAdmin peut-être installé de manière interactive en appelant des fichiers php d'administration. Ils sont une faille et doivent être supprimés. Utiliser le module `file` et supprimer les fichiers `/usr/share/phpmyadmin/test` et `/usr/share/phpmyadmin/setup`

9. C'est terminé ! Ou presque :) Au vu du nombre de modification faite dans le repertoire `/usr/share/phpmyadmin`, il serait dommage de supprimer cette configuration lors de la phase d'installation et de PhpMyadmin.

   Retournez dans le fichier `tasks/main.yml` et ajoutez comme première tâche un moyen de vérifier si le dossier `/usr/share/phpmyadmin` existe. Penchez-vous du côté du module `ansible.builtin.stat`.

   Si le dossier existe alors utiliser `when` afin de ne pas lancer l'installation de phpmyadmin. La configuration peut, elle, se réaliser car il est possible que de nouveaux serveurs de BDD soient à y ajouter. (C'est une sécurité en plus des tags ajoutés plus tôt)