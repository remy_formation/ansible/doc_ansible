Formation ansible avec toutes les ressources (powerpoint + exercice)

Arborescence :

- exercices : Contiens les différents exercices associés à la formation. Attention, les .md sont "source de vérité", il peut y avoir des .pdf mal mis à jour.
- images: contient les ressources 
- local (en .gitignore): je l'utilise pour créer l'archive que je vais envoyer aux formés avec la clé SSH, le mdp du vscode, les adresses IP, les slides & les exercices
- media : contient une image utilisé quelque part dans le ppt... à virer un jour
- ressources : documents annexes, comme des notes pour le live coding, des fiches de suivie de formation...

Et les slides sont à la racine

Merci à Nicolas Vallet pour sa contribution, en fournissant les exercices sur MyPhpAdmin et les bases de données MariaDB
