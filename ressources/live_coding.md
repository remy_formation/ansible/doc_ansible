```bash Init un role "Ping" avec ansible-galaxy :
### clean host
rm ~/.ssh/known_hosts

### Init ansible galaxy
mkdir roles
cd roles
ansible-galaxy init ping
cd ..

### inventory
mkdir inventory
echo "
[mon_groupe]
localhost ansible_ssh_private_key_file="~/.ssh/ansible_key"
" > inventory/test.ini

### Validation
ansible mon_groupe -i inventory/test.ini -m ping

### playbook
echo "
- name: Mon super playbook
  hosts: mon_groupe
  roles:
    - ping
" > playbook_livecode.yml

### Customisation du role 
# Definition des taches
echo '
---
- name: enrobage du ping
  ping:
    data: "{{ my_super_var }}"
' > roles/ping/tasks/ping.yml

# Invocation de celle-ci dans le tasks/main.yml
echo "
---
- name: un debug inutile
  debug:
    msg: "Bonjour !"
    
- include_tasks: ping.yml
" > roles/ping/tasks/main.yml

# Import est statique => on l’utilise pour la logique, pré calculé, include dynamique => Si on crée des task à la volée

# Ajout d'une variable par defaut
echo "
---
my_super_var: "Some value"
" > roles/ping/defaults/main.yml

### Run playbook 1

ansible-playbook -i inventory/test.ini playbook_livecode.yml

### Group vars
mkdir -p group_vars/mon_groupe
echo "
my_super_var: crash
" > group_vars/mon_groupe/default.yml

### Run playbook 2

ansible-playbook -i inventory/test.ini playbook_livecode.yml

```



