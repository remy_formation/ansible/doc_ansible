# Préparation à l’atelier ansible

But du guide : avoir à disposition deux machines virtuels capable de communiquer entre elle par SSH. Je propose un guide pour le faire sur Virtual box. Vous pouvez éventuellement le faire avec une autre technologie si vous préférez, mais je ne pourrais peut-être pas vous aider le jour J en cas de soucis de réseau.

ansible-serveur : VM qui porte Ansible

ansible-client : VM qui sera piloté par ansible-serveur

## Préparation des VM sur Vbox

Créer une VM : 
Ubuntu 20.04.4
2Go de mémoire
10Go de stockage
(Installation minimale, un peu plus de stockage ne fait pas de mal)

En hostname : ansible-client

(On fait quelques configurations puis on clonera la VM pour faire le serveur)

### Réseau entre VM

#### Créer le réseau

Sur Vbox, outils
Dans Outils > Paramètres
Réseau, créer un nouveau, le renommer Ansible

#### Réseau de la VM

Clic droit > paramètres > Réseau
Onglet Adapter 1, Choisir Réseau NAT, Ansible

Note : La VM doit être éteinte

#### Ajouter le copié collé

Périphérique > Installer l’image CD des additions invités
Sur la VM, Lancer l'installation,, mettre le mdp utilisateur
=> On redémarrera plus tard
Périphériques > Presse-papier partagé > Bidirectionnel

#### Installer quelques outils

```bash
Sudo apt update
Sudo apt install vim ssh -y
```
Note : vous pouvez mettre l'éditeur de votre choix, nano etc...

#### Clonage

Clic droit > Cloner
On donnera au clone le nom d'« ansible-serveur »
Politique d’adresse MAC > Générer de nouvelles adresses MAC…
Clone intégral

#### Les hostnames

Sur les deux machines :
```bash
Sudo vim /etc/hostname
```

"ansible-client" doit être correctement positionné
Sur "ansible-serveur" il faut corriger le hostname 

Puis on met à jour le /etc/hosts pour mettre à jour le hostname et ajouter l’autre machine
```bash
#Récupérer l’ip
ip addr

#Fichier à mettre à jour
Sudo vim /etc/hosts
```

```bash
#changer le 127.0.1.1 pour le hostname
127.0.1.1    ansible_XXX

# Ajouter la ligne serveur sur le client, et client sur le serveur
10.0.2.X    ansible_XXX
```

Exemple de config finale 
- pour le client (qui a pour ip sur le réseau ansible 10.0.2.5) : 
```bash
127.0.0.1 localhost
127.0.1.1 ansible-client
10.0.2.4 ansible-serveur
```
- pour le serveur (qui a pour ip sur le réseau anvile 1.0.2.4) : 
```bash
127.0.0.1 localhost
127.0.1.1 ansible-serveur
10.0.2.5 ansible-client
```

Pour valider, vous devriez être en mesure de ping une machine depuis l'autre.

## Installation d’Ansible et config ssh

### Ansible
Sur la VM serveur, tout simplement :
```bash
Sudo apt install ansible -y
```

Dans le fichier /etc/ansible/hosts (fichier hosts par défaut d’ansible), rajouter :
```yaml
[atelier]
ansible-client
ansible-serveur

[client]
ansible-client
```

Ensuite on peut tester qu’ansible communique bien avec les VMs (mais attention, on n'a pas encore configuré le ssh donc c’est normale d’avoir des erreur de permission denied)
```bash
ansible atelier -m ping
```

L'erreur doit être un "permission denied", autrement c'est qu'une erreur de configuration est présente.
```bash
remy@ansibleclient:~$ ansible atelier -m ping
ansible-client | UNREACHABLE! => {
  "changed": false,
  "msg": "Failed to connect to the host via ssh: remy@ansible-client: Permission denied (publickey,password).",
  "unreachable": true
}

ansible-serveur | UNREACHABLE! => {
  "changed": false,
  "msg": "Failed to connect to the host via ssh: remy@ansible-serveur: Permission denied (publickey,password).",
  "unreachable": true
}
```


### SSH

Sur la vm serveur, générer les clés :
```bash
Cd ~/.ssh

#Quand l’invite demandera un nom, mettre ansibleserveur
#pour les passphrase, laisser vide et faire « entrer »
sh-keygen -t rsa -b 4096
```

Depuis le serveur, copier la clé publique sur le client, ex :
```bash
cd ~/.ssh

#remplacer le user par le votre
scp ansibleserveur.pub remy@ansible-client:~/.ssh/ansibleserveur.pub
```


Sur les deux VMs, ajouter la clé aux authorized_keys :
```bash
cd ~/.ssh
cat ansibleserveur.pub >> authorized_keys
```


Et maintenant, Ansible devrait réussir à se connecter sur les deux machines :
```bash
remy@ansibleclient:~/.ssh$ ansible atelier -m ping
ansible-serveur | SUCCESS => {
  "ansible_facts": {
    "discovered_interpreter_python": "/usr/bin/python3"
  },
  "changed": false,
  "ping": "pong"
}
ansible-client | SUCCESS => {
  "ansible_facts": {
    "discovered_interpreter_python": "/usr/bin/python3"
  },
  "changed": false,
  "ping": "pong"
}
```